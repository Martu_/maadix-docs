Usuarixs
========

Categorías de usuarixs
----------------------

En MaadiX existen diferentes categorías de cuentas que puedes activar o
administrar en tu sistema. Cada una cuenta con distintos privilegios y
funciones, que en esta página te ayudaremos a entender para saber mejor
cómo utilizarlas.

-  **Admin del panel de control**: se trata de la única cuenta con
   privilegios ilimitados para la administración de los datos a través
   del panel de control. Esta cuenta se crea automáticamente en el mismo
   momento en el que se crea el servidor, y no se puede eliminar.
-  **Superusuarix**: aunque esta cuenta no tenga acceso a través del
   panel de control, es jerárquicamente más importante que la anterior,
   dado que tiene privilegios ilimitados sobre la totalidad del sistema
   ("cuenta root" de Linux). No se puede eliminar, de modo que no
   llegues a perder nunca el acceso root al sistema.
-  **Cuentas ordinarias**: cuentas que la cuenta admin puede crear a
   través del panel de control, a las cuales puede otorgar permisos para
   utilizar o acceder a los distintos servicios.
-  **Cuentas postmaster**: por cada dominio activado en el panel de
   control, se crea una cuenta postmaster. Este tipo de cuenta tendrá la
   posibilidad de crear y administrar cuentas de correo electrónico
   asociadas al mismo dominio.
-  **Cuentas de correo**: las cuentas de correo electrónico tendrán
   acceso al panel de control con el único privilegio de ver o editar
   sus propios datos (contraseña, nombre, reenvío automático...).

Admin del panel de control
--------------------------

La cuenta con permisos de administración del panel de control es
probablemente la cuenta que más utilizarás, ya que tiene todos los
permisos dentro del panel de control. Esto significa que esta cuenta
puede crear, configurar y eliminar a todas las demás así como los
dominios, las cuentas de correo electrónico y cualquier otro dato
administrable desde el panel de control.

Esta cuenta se crea automáticamente y no es posible eliminarla. De lo
contrario, perderías el acceso al panel de control. Cuando desde MaadiX
construimos el servidor, también creamos esta cuenta, asignándole una
contraseña aleatoria que por razones de seguridad tendrás que cambiar la
primera vez que accedes al panel de control.

Todos los parámetros, excepto el nombre de usuarix, se pueden editar en
cualquier momento desde la página de 'Perfil' del panel de control.
Puedes acceder a ella a través del menú que se despliega en la esquina
superior derecha.

.. figure:: img/users/edit-profile.png
   :alt: admin

   Detalles de la cuenta admin del panel de control.

Es muy importante que el correo electrónico asociado a esta cuenta sea
válido y que tengas acceso a él, puesto que es el sistema lo utiliza
para enviar ciertas notificaciones o funcionalidades importantes, tales
como la recuperación de la contraseña.

Superusuarix
------------

Esta cuenta no tiene acceso al panel de control. Se trata de la cuenta
root que puede operar a través de la terminal (por SSH) y que tiene
ilimitados privilegios sobre la totalidad el sistema. Por esta razón es
jerárquicamente más importante que la anterior, dado que tiene la
facultad de editar y borrar cualquier archivo. Esta cuenta no puede ser
eliminada.

Por razones de seguridad, es aconsejable limitar el uso de esta cuenta
cuando sea posible y no compartirla con personas ajenas. En su lugar, se
recomienda usar cuentas ordinarias con accesos limitados al sistema. En
este mismo documento encontrarás más detalles sobre cómo crear y
utilizar este tipo de cuentas.

Al igual que la cuenta administración del panel de control, la cuenta
Superusuarix se crea automáticamente en el proceso de creación del
servidor, y hasta que no cambies su contraseña durante el proceso de
activación del panel de control, será una cuenta inactiva, sin ningún
tipo de acceso.

Una vez activada, esta cuenta podrá acceder al servidor a través de una
conexión SFTP o SSH. Sus privilegios en el sistema son ilimitados, de
forma que le permiten efectuar cualquiera de las siguientes operaciones,
que no están permitidas desde el panel de control:

-  Acceder a todas las carpetas del sistema a través una conexión STFP o SSH.
-  Modificar, instalar o eliminar cualquier aplicación (SSH).
-  Leer, modificar, eliminar o ejecutar cualquier archivo del sistema, incluso si no es propietarix (SSH).
-  Crear otras cuentas 'root'.
-  Apagar o reiniciar el servidor.

Cuentas ordinarias
------------------

Son cuentas creadas por la cuenta admin del panel de control y a las que
se pueden asignar diferentes grados de permisos y accesos.

-  **Acceso solo por SFTP**: podrán acceder al servidor solo por SFTP y
   estarán confinadas en su propia carpeta personal. No pueden acceder
   al resto del sistema ni tienen acceso SSH. Se les puede asignar como
   webmasters de uno o varios dominios.
-  **Acceso por SSH y SFTP**: podrán acceder al servidor tanto con un
   cliente SFTP como por SSH. Al contrario de las cuentas anteriores no
   estarán confinadas en su directorio y podrán acceder a otras rutas
   para las que se les de permiso. Se les puede asignar como webmasters
   de uno o varios dominios.
-  **Cuentas phpMyAdmin**: se les puede activar el permiso para acceder
   a esta aplicación, en caso de que estuviera instalada. Por razones de
   seguridad, esta aplicación está protegida por una doble contraseña:
   1. la de una cuenta con acceso phpMyAdmin y 2. La de un usuarix mysql
   con acceso a bases de datos.
-  **Cuentas VPN**: se les puede activar una cuenta VPN para que su
   conexión con el servidor sea directa y segura. Asimismo, podrán
   utilizar esta conexión para conectarse a cualquier otra dirección de
   Internet.
-  **Cuentas Jitsi**: se les puede activar una cuenta para que puedan
   abrir salas de videollamadas en Jitsi, si se tiene la aplicación
   Jitsi instalada.

Cuenta postmaster
-----------------

Esta cuenta solamente tiene la facultad de administrar las cuentas de
correo electrónico asociadas a su dominio. Podrá crear, modificar y
borrar las direcciones de correo a través del panel de control. Este
tipo de cuenta es sumamente útil para poder permitir estas tareas sin
necesidad de otorgar a otra persona todos los privilegios.

Las cuentas postmaster se crean por defecto por cada dominio que se
habilita en el panel de control con una contraseña aleatoria que se
puede editar a posteriori.

Para operar desde el panel de control, las cuentas postmaster tendrán
que identificarse utilizando el nombre de usuarix
postmaster@sudominio.com y la contraseña que se le ha asignado.

.. figure:: img/users/postmaster-login.png
   :alt: Postmaster log in
   :scale: 80%

   Acceso de Postmaster.

Una vez dentro del panel de control, las cuentas postmaster tendrán una
interfaz ligeramente distinta a la que ve la cuenta admin del panel del
control, en la que sólo tendrán habilitado el acceso a las
funcionalidades de edición de las cuentas de correos asociadas a su
dominio así como la edición de su propio perfil.

.. figure:: img/users/postmaster-logged.png
   :alt: Postmaster logged in

   Panel del control accedido con Postmaster.

Cuentas de correo
-----------------

Las cuentas de correo electrónico creadas en el panel de control pueden
ser administradas directamente por sus titulares. La persona que tenga
un correo electrónico activado, podrá entrar en el panel de control
insertando su cuenta de correo como nombre de usuarix y la contraseña
asignada a la misma.

.. figure:: img/users/email-login.png
   :alt: Email log in
   :scale: 80%

   Acceso con cuenta de correo.

Sus privilegios de edición están limitados a su propia cuenta de correo
electrónico. Podrá cambiar la contraseña y su nombre y apellido, además
de establecer o desactivar el reenvío y las respuesta automática.
También puede acceder para consultar los datos para la configuración de
su cuenta en un cliente de correo electrónico en su dispositivo
(Thunderbird, Outlook...).

.. figure:: img/users/email-logged.png
   :alt: Email logged in

   Detalles de la cuenta de correo.
