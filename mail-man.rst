Mailman
=======

Introducción
------------

Mailman es un software que permite crear y administrar listas de correo
electrónico y boletines (newsletter), el cual comprende tres diferentes
interfaces web diferentes.

-  **Administración**
-  **Gestión de listas**
-  **Archivos de listas**

Cuando la instalación de la aplicación de Mailman en tu servidor
termine, **se te enviará un correo electrónico que incluye los datos de
acceso** de la cuenta de administración de la aplicación.

Podrás acceder a la interfaz de Mailman en
https://myserver.maadix.org/mailman o en
https://myserver.example.com/mailman (si has configurado tu propio
domino para el servidor).

Cambiar contraseña
------------------

Como para todas las demás aplicaciones que instales en el servidor, lo
primero que harás es cambiar la contraseña de la cuenta de
administración por razones de seguridad.

Para hacerlo, haz clic en el icono de cuenta arriba a la derecha y
selecciona 'Cuenta'. Se abrirá una página que te permitirá cambiar la
contraseña, además de configurar otros parámetros.

.. figure:: img/mailman/edit-account.png
   :alt: Change password

   Cambiando la contraseña.

Configurar dominio
------------------

Las listas de correo funcionan bajo un dominio o subdominio propio. Por ello, necesitarás tener un dominio propio y acceso a su 'editor de zonas DNS' (Operación externa a MaadiX: **recuerdad que MaadiX no es un proveedor de dominios ni ofrece un editor de zonas DNS**).

Además, tienes que recordar que **no puedes crear listas de correo
utilizando el mismo dominio que estás utilizando para cuentas de email**,
esto podría causar problemas. Es decir, en el caso que estés utilizando
el dominio example.com para cuentas de correo electrónico
(admin@example.com, info@example.com....), no podrás utilizar el mismo
para crear listas de correo. Lo que tendrás que hacer es crear un
subdominio de example.com (por ejemplo, listas.example.com) y configurar
los DNS para este dominio o subdominio antes de empezar a crear listas.

Activar dominio o subdominio en Mailman
---------------------------------------

Podrás acceder a la interfaz de Mailman en
https://myserver.maadix.org/mailman o en
https://myserver.example.com/mailman (si has configurado tu propio
domino para el servidor).

Después de hacer login (pestaña 'Login' arriba a la derecha), aparecerá
la pestaña 'Domains' en el menú superior. Accede a esta página y haz
clic en el botón 'Add Domain' (Añadir dominio).

Como ejemplo utilizaremos listas.example.com, pero puedes asignar el
nombre que prefieras (news.example.com, info.example.com, etc).

.. figure:: img/mailman/add-domain.png
   :alt: Add\_domain

   Añadiendo un nuevo dominio.

Tendrás que rellenar los tres campos siguientes:

-  **Mail Host**: el nombre del dominio o subdominio bajo el cual se
   podrán crear listas (en nuestro caso listas.example.com, por el que
   tendremos que crear luego las entradas DNS correspondientes).
-  **Description**: una descripción para el dominio o subdominio
   (opcional).
-  **Alias Domain**: Puedes dejarlo vacío.
-  **Web Host**: la dirección web en la que será posible acceder a las
   opciones para las listas de este subdominio. Puedes dejar el valor
   por defecto myserver.maadix.org o bien añadir otro dominio o
   subdominio, siempre que esté activado en el panel de control y
   funcione correctamente. Esta será la dirección web que se enviará a
   los suscriptores para acciones tales como confirmar suscripción,
   consultar listas públicas u otras.

En el caso de este ejemplo, el **Web Host** podría ser el dominio
example.com o incluso el mismo subdominio *listas.example.com*, que
deberá tener los DNS correctamente configurados para que apunten a tu
servidor, y tendrá que estar activado en el panel de control. Si decides
utilizar el mismo dominio o subdominio de las listas como dirección para
la interfaz gráfica, es muy importante que no actives el servidor de
correo para él. Deberás entonces activar el subdominio
*listas.example.com* en el panel de control, dentro de la sección
'Añadir Dominios', sin marcar la casilla 'Activar servidor de correo
para este dominio'.

De esta forma, se creará la configuración necesaria para que el
subdominio *listas.example.com* se pueda visitar desde el navegador, sin
que se active el servidor de correo. Si activaras el servidor de correo
para un dominio que quisieras utilizar para las listas, podrías
experimentar problemas en la entrega de los mensajes.

Activar DKIM
------------

Accede al panel de control y consulta la pestaña Mis Aplicaciones >
Mailman > Dominios de listas en el menú de la columna izquierda. Te
aparecerá un listado de todos los dominios añadidos desde Mailman. Desde
esta misma página puedes activar la clave DKIM para los dominios que
hayas creado desde la interfaz de Mailman. Recuerda que la clave DKIM es
importante para evitar que los correos enviados acaben en la carpeta
SPAM. Puedes consultar más información sobre DKIM en la página `DNS -
Registro Dkim </dns/#registro-dkim>`__.

.. figure:: img/mailman/activate-dkim.png
   :alt: Activar Dkim

   Activando clave DKIM.

Configurar DNS para el dominio o subdominio
-------------------------------------------

Para que las listas creadas funcionen correctamente, es necesario
configurar los DNS para el dominio o subdominio añadido.

Como primer paso, necesitas acceder a través del panel de control a los
valores requeridos para los registros DNS. El panel de control detectará
automáticamente los dominios o subdominios activados desde Mailman y te
devolverá los valores correctos para las entradas DNS, que tendrás que
configurar en el panel que te proporciona tu proveedor de dominio
(operación externa a MaadiX).

Desde la página Mis Aplicaciones > Mailman > Dominios de listas en el
menú de la columna izquierda accedes al listado de todos los dominios
añadidos desde Mailman.

Haz clic en el enlace 'Ver' de la columna DNS del dominio o subdominio
que quieras configurar.

Aquí encontrarás los valores DNS necesarios para una correcta
configuración. Si solamente quieres usar este dominio o subdominio para
listas de correo, será suficiente que configures los DNS para los
valores MX, TXT y DKIM, que encontrarás en la tabla 'Servidor de
Correo'.

.. figure:: img/mailman/required-dns.png
   :alt: Listado dominios mailman

   Registros DNS necesarios.

Para crear un subdominio o configurar los DNS de un dominio existente,
tendrás que entrar en el panel de administración que te proporciona tu
proveedor (Gandi, Dinahosting, etc...). Lamentablemente, cada interfaz
es diferente dependiendo del proveedor, de modo que no existe una manera
única de llevar a cabo este proceso. Consulta la sección `DNS </dns>`__
de esta guía de uso para más información.

Crear listas de correo o boletines
----------------------------------

Una vez añadido un dominio a la aplicación Mailman, puedes crear una o
más listas de correo o boletines bajo el mismo dominio. Para hacerlo,
visita la pestaña 'Lists' en el menú de arriba y luego haz clic en 'Add
new List'.

.. figure:: img/mailman/add-list.png
   :alt: Add\_list

   Creando listas de correo.

Tendrás que rellenar los siguientes campos:

-  **List Name**: el nombre de la lista. En el ejemplo, crearemos la
   lista 'info'.
-  **Mail Host**: el dominio para la listas. En el desplegable escogemos
   el subdominio recién creado lists.example.com.
-  **Initial list owner address**: asignamos una cuenta de correo como
   propietaria / administradora de la lista.
-  **Advertise this list?**: esta opción establece si la lista será
   visible públicamente en el listado de listas creadas o no. Para que
   se muestre, elegiremos 'Advertise this list in list index', mientras
   que si elegimos 'Hide this list in list index' solamente la verá la
   persona administradora.
-  **List Style**: el tipo de lista que quieres crear. Las tres opciones
   que puedes elegir aquí son: "Lista de correo que solo hace anuncios"
   (*Announce only mailing list style*); "Lista de correo de discusión
   clasica" (*Ordinary discussion mailing list style*); o "lista de
   correo de discusión con archivos privados"(\ *Discussion mailing list
   style with private archives*).
-  **Description**: una descripción informativa de la lista (opcional).

Una vez completada esta fase, ya habremos creado la lista. A
continuación, tendremos que establecer nuestras preferencias de
configuración.

**Establecer parámetros del archivo de la lista**

Puedes elegir si los mensajes enviados a los suscriptores se conservan y
almacenan y, en tal caso, si serán visibles por cualquier persona a
través de un enlace público o solamente serán accesibles a personas
autorizadas.

En Settings > Archiving > Archive policy:

-  **Public archives**: cualquiera puede consultarlos a través del
   interfaz web de tu instalación de la aplicación.
-  **Private archives**: el historial se guarda, pero solamente las
   cuentas administradoras pueden acceder a él.
-  **Do not archive this list**: no se guarda el historial de mensajes
   enviados.

.. figure:: img/mailman/mailman-archive.png
   :alt: Archive\_policy

   Política de archivado de mensajes.

El archivo de las listas lo podrás consultar en
https://myserver.maadix.org/hyperkitty o en
https://myserver.example.com/hyperkitty (si has configurado tu propio
domino para el servidor).

**Notificación de cambios en la suscripción**

En Settings > Automatic Responses > Notify admin of membership changes,
marca la casilla si quieres que se notifique al administrador de las
altas y las bajas del boletín.

.. figure:: img/mailman/list-changes.png
   :alt: List\_changes

   Opciones de notificación.

**Añadir Templates (plantillas)**

En Templates se pueden añadir plantillas para diferentes mensajes que
Mailman trae por defecto.

Por ejemplo, si se quiere cambiar el mensaje de bienvenida a la lista,
se puede añadir una plantilla nueva haciendo clic en "New Template" y
seleccionando del desplegable "[list:user:notice:welcome]". En el campo
Data se puede añadir el mensaje de bienvenida que se quiera.

.. figure:: img/mailman/welcome.png
   :alt: template\_bienvenida

   Plantilla de bienvenida.

Si se quisiera cambiar el mensaje al pie (footer) que se añade a cada
mail que se envía a la lista, se puede seleccionar del desplegable
"[list:member:regular:footer]" y en el campo Data añadir el texto que se
desee.

.. figure:: img/mailman/footer.png
   :alt: template_footer

   Plantilla de mensaje al pie.

Una vez creada la lista, debes seguir un proceso de configuración
específico para convertirla en un boletín (newsletter) o una lista de
correo.

Configurar un boletín (Newsletter)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Recuerda que el boletín se caracteriza por su unidireccionalidad:
solamente las cuentas expresamente autorizadas pueden enviar mensajes
(en oposición a las listas de correo, en las que todo el mundo puede
participar).

**Establecer parámetros de suscripción y baja del boletín**

En 'Settings' > 'Member Policy', elige una de las siguientes opciones
del desplegable para 'Subscription Policy':

-  **Open**: cualquiera puede ser añadido al boletín sin precisar su
   confirmación.
-  **Confirm**: las suscripciones tienen que ser confirmadas desde un
   correo válido al solicitar el alta.
-  **Moderate**: un moderador tiene que autorizar manualmente cada alta.
-  **Confirm then Moderate**: enviar primero un correo de confirmación a
   la persona para que luego un moderador autorice el alta.

Aconsejamos la opción 'Confirm' para que no se puedan añadir direcciones
de correo sin consentimiento.

De igual manera se pueden elegir las mismas opciones para la
'Un-Subscription Policy', que se aplicarán cuando las personas quieran
darse de baja del boletín.

.. figure:: img/mailman/sub-policy.png
   :alt: Sub\_policy

   Opciones de política de suscripción.

**Establecer parámetros para los mensajes entrantes**

Determina cómo se tratan los mensajes dirigidos a la lista por parte de
las personas miembras y no miembras. Puesto que se trata de un boletín,
se denegará la entrega de cualquier mensaje entrante cuyo remitente no
esté autorizado.

En Settings > Message Acceptance, define los siguientes parámetros:

-  **Default action to take when a member posts to the list: Discard**
   (se descartarán todos los mensajes provenientes de personas
   suscritas).
-  **Default action to take when a non-member posts to the list:
   Discard** (se descartarán todos los mensajes provenientes de personas
   no suscritas).

Con esta configuración, cualquier mensaje dirigido a la lista por parte
de un remitente no autorizado será automáticamente rechazado sin
notificar a la persona. Si prefieres que reciban una notificación,
sustituye la opción por 'Discard por Reject (with notification)'.

.. figure:: img/mailman/inbox-messages.png
   :alt: Inbox\_messages

   Configuración para mensajes entrantes.

**Configuración de los envíos de boletines**

En este momento, ninguna persona está autorizada a enviar correos, así
que es necesario conceder este permiso a al menos una cuenta de correo,
para poder hacer los envíos.

En 'Users' > 'Members' encuentras el listado de suscriptores. Escoge la
cuenta de correo que quieres utilizar para enviar las newsletter, y haz
clic en 'Member Options'. Si la cuenta no está presente en el listado,
tendrás primero que añadirla ('Mass operations' > 'Mass suscribe').

Al final de la página, en para el parámetro 'moderation' selecciona
'Accept immediately', para que la cuenta seleccionada pueda enviar los
boletines. Recuerda que puedes autorizar más de una cuenta para la misma
lista.

.. figure:: img/mailman/send-newsletter.png
   :alt: Newslettering

   Opciones de moderación.

Configurar una lista de correo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Una vez creada la lista, debes seguir un proceso de configuración
específico para convertirla en una lista de correo ('Mailing list').
Recuerda que la lista de correo se caracteriza por ofrecer a todos las
personas la posibilidad de participar (no es unidireccional como el
boletín).

**Establecer parámetros de suscripción y baja**

En 'Settings' > 'Member Policy', elige una de las siguientes opciones
del desplegable:

-  **Open**: cualquier correo electrónico puede ser añadido a la lista
   sin precisar su confirmación.
-  **Confirm**: las suscripciones tienen que ser confirmadas desde un
   correo válido al solicitar el alta.
-  **Moderate**: un moderador tiene que autorizar manualmente cada alta.
-  **Confirm then Moderate**: enviar primero un correo de confirmación
   para que luego una persona moderadora autorice el alta.

Aconsejamos la opción 'Confirm' para que no se puedan añadir direcciones
de correo sin consentimiento.

De igual manera se pueden elegir las mismas opciones para la
'Un-Subscription Policy', que se aplicarán cuando las personas quieran
darse de baja de la lista.

.. figure:: img/mailman/sub-policy.png
   :alt: List\_settings

   Opciones de política de suscripción.

**Establecer parámetros para los mensajes entrantes**

Para las listas de correo, en las que las personas pueden mantener
conversaciones entre ellas, puedes determinar cómo se tratan los
mensajes dirigidos a la lista, tanto por parte de las personas suscritas
como por parte de las que no lo están.

En 'Settings' > 'Message Acceptance':

Para personas suscritas, elige entre una de estas dos opciones dentro de
'Default action to take when a member posts to the list':

-  **Hold for moderation**: una persona moderadora tiene que autorizar
   cualquier mensaje para que sea entregado al resto de personas
   suscritas a la lista.
-  **Accept immediately**: los mensajes de las personas suscritas se
   entregarán automáticamente, sin necesidad de moderación. Esta es la
   opción estándar para las listas de correo.

Para personas no suscritas, elige la opción que prefieras dentro de
'Default action to take when a non-member posts to the list':

-  **Hold for moderation**: una persona moderadora tiene que dar su
   autorización para que el mensaje sea entregado a los suscriptores de
   la lista. Esta es la opción estándar para las listas de correo.
-  **Reject**: se rechazarán automáticamente todos los mensajes,
   notificando a la remitente.
-  **Discard**: se rechazarán automáticamente todos los mensajes, sin
   notificar a la remitente.
-  **Accept immediately**: se aceptarán automáticamente todos los
   mensajes, sin necesidad de moderación.

.. figure:: img/mailman/lists-policy.png
   :alt: Lists\_policy

   Opciones de moderación de mensajes.
