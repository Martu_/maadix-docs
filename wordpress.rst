WordPress
=========

Instalación
-----------

1. Descarga la aplicación desde la `página oficial <https://wordpress.org/download/>`__  y descomprime la carpeta.

.. figure:: img/wordpress/download-wordpress.png
   :alt: Screenshot

   Descarga de WordPress desde su web.


2. Sube al servidor los archivos de la carpeta "wordpress", dentro de la carpeta del dominio que se ha generado al activar el dominio desde el panel del control. Si has añadido el dominio *example.com*, la carpeta será ``/var/www/html/example.com/``.

Para hacerlo, puedes utilizar un cliente SFTP como Filezilla. Recuerda `aquí </create-web>`__ como subir contenidos al directorio ``/var/www/html/example.com/``. **Advertencia: No debes subir la carpeta "wordpress" como tal, únicamente los archivos que contiene.**

3. Crea la base de datos.

Para crear una base de datos, puedes utilizar la aplicación phpMyAdmin. Recuerda como usar phpMyAdmin `aquí </mysql>`__.

Importar una base de datos existente
------------------------------------

En caso de que ya tengas una base de datos MySQL de tu WordPress (por
ejemplo, en casos de migración desde otro servidor), es necesario
importarla. Desde phpMyAdmin, tienes que seleccionar el apartado
"Importar" y subir el archivo que corresponda (.sql o .gzip, .bzip2 o
.zip en el caso esté comprimida).

.. figure:: img/wordpress/importar-bdd.png
   :alt: Screenshot

   Importando base de datos en phpMyAdmin.

Una vez importada, tenemos que seleccionar el apartado "Privilegios"
para crear una nueva cuenta que pueda acceder a la base de datos, leer y
escribir en ella (se desaconseja usar la misma cuenta root o admin por
razones de seguridad).

Elige un nombre y una contraseña y haz clic en "Otorgar todos los
privilegios para la base de datos "MiBaseDeDatos"". Apunta el nombre de
la cuenta y la contraseña que acabas de crear, puesto que serán
necesarios durante el proceso de instalación de WordPress.

.. figure:: img/wordpress/otorgar-privilegios.png
   :alt: Screenshot

   Otorgando privilegios para base de datos en phpMyAdmin.

Crear una nueva base de datos
-----------------------------

Si se trata de una nueva instalación y todavía no tenemos creada una
base de datos MySQL, es preciso crear una. Dentro de phpMyAdmin, tenemos
que ir a "Bases de datos" > "Crear base de datos".

.. figure:: img/wordpress/crear-bdd.png
   :alt: Screenshot

   Creando una base de datos en phpMyAdmin.

Una vez creada, tenemos que seleccionar el apartado "Privilegios" para
crear una nueva cuenta que pueda acceder a la base de datos, leer y
escribir en ella (se desaconseja usar la misma cuenta root o admin por
razones de seguridad).

Elige un nombre y una contraseña y haz clic en "Otorgar todos los
privilegios para la base de datos "MiBaseDeDatos"". Apunta el nombre de
la cuenta y la contraseña que acabas de crear, puesto que serán
necesarios durante el proceso de instalación de WordPress.

.. figure:: img/wordpress/otorgar-privilegios.png
   :alt: Screenshot

   Otorgando privilegios para base de datos en phpMyAdmin.

Configurar WordPress
--------------------

Cuando tengamos creada la base de datos y los archivos de la carpeta
WordPress ya estén en ``/var/www/html/example.com/``, tenemos que
visitar el dominio con el navegador. Aparecerá un formulario para
finalizar el proceso de instalación de WordPress, en el que se
solicitarán las credenciales de la cuenta de MySQL que hemos creado.

.. figure:: img/wordpress/formulario-wp.png
   :alt: Screenshot

   Asistente de instalación de WordPress.

Recuerda que cambiar el prefijo wp\_ por otro en el apartado "Table
Prefix" añade una capa de seguridad adicional a tu instalación.

Problemas frecuentes en la instalación de WordPress
---------------------------------------------------

No puedo instalar plugins ni subir imágenes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Asegúrate de que los permisos de los archivos que has subido por SFTP
sean los correctos. Para que WordPress pueda subir imágenes y plugins
desde el panel de administración, la carpeta
``/var/www/html/example.com/wp-content`` tiene que tener los siguientes
permisos:

``drwxrws---`` es decir, 770

Para comprobar que los permisos sean correctos, desde FileZilla debes
hacer clic con el botón derecho encima de la carpeta ``wp-content`` y
seleccionar la opción "Permisos de archivo" o "Atributos de archivo",
según tu versión.

Los permisos tienen que quedar de esta manera, siendo el valor numérico
770. Esto quiere decir que tanto la cuenta Webmaster como Apache podrán
escribir en la carpeta. Si los permisos que tienes son diferentes,
puedes proceder a modificarlos desde el mimso Filezilla, asignando dos
permisos diferentes para carpetas y para archvios. Para las **carpetas**
es necesario el permiso de ejecución. Pondrás entonces el **permiso
770** y marca la opción **"Aplicar solo a directorios"**.

.. figure:: img/wordpress/permisos-carpetas.png
   :alt: Screenshot

   Edición de permisos de carpetas.

Repite la operación para los **archivos**, que no necesitan permiso de
ejecución. Puedes asignar **permisos 660** y seleccionar la opción
"**Aplicar solo a ficheros"**.

.. figure:: img/wordpress/permisos-archvios.png
   :alt: Screenshot

   Edición de permisos de archivos.

Si quieres habilitar la opción de actualizar todo el wordpress desde el
panel web de administración (wp-admin) tendrás que seguir los mismos
pasos pero aplicando estos cambios no solo a la carpeta ``wp-content``,
sino a todas las carpetas del wordpress, es decir
``/var/www/html/example.com/``

--

A pesar de que los permisos de las carpetas del WordPress sean los
correctos, podrías tener problemas a la hora de instalar plugins o subir
imágenes. Para solucionarlo, añade la siguiente línea al final del
archivo ``wp-config.php``:

``define( 'FS_METHOD', 'direct' );``

Cuando subes imágenes o plugins desde WordPress, en lugar que desde un
cliente SFTP, quién ejecuta la operación no es tu propia cuenta, sino
Apache (www-data). A pesar de que esta cuenta tenga permisos de
escritura sobre los archivos, no es la propietaria. En servidores
compartidos, esto podría representar un problema de seguridad, ya que la
misma cuenta www-data podría estar siendo utilizada por todas las
personas que tengan acceso. Por ello, WordPress utiliza un método en el
que comprueba (además de los permisos) si la propietaria de los archivos
coincide con la cuenta que está cumpliendo la operación de escritura.
Situación que no se da, por lo que no sigue con la operación y te
solicita las credenciales FTP.

En MaadiX no tienes FTP ni FTPs, únicamente SFTP , que utiliza el puerto
22 en lugar del 21, que es el que utiliza WordPress sin darte la opción
de cambiarlo. La solución de definir el método como 'direct' hace que
WordPress acceda de forma directa al sistema de archivos, en el que la
cuenta www-data tiene los permisos necesarios para escribir.

Si quieres saber más, puedes consultar estos dos enlaces:

-  `https://wordpress.stackexchange.com/questions/189554/what-security-concerns-should-i-have-when-setting-fs-method-to-direct-in-wp-co <https://wordpress.stackexchange.com/questions/189554/what-security-concerns-should-i-have-when-setting-fs-method-to-direct-in-wp-co>`__

-  `https://wordpress.stackexchange.com/a/232291 <https://wordpress.stackexchange.com/a/232291>`__
