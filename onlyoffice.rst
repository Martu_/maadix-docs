OnlyOffice
===========

OnlyOffice es una aplicación que, como Libre Office Online, proporciona
la posibilidad de editar de forma colaborativa y en tiempo real,
documentos en un navegador.

Constituye una alternativa a Google Docs, y permite editar documentos de
texto enriquecido, hojas de cálculo y presentaciones. Por defecto
utiliza el formato .docx, pero se pueden habilitar otros formatos para
archivos creados fuera de la aplicación y subidos a nuestra instalación
(.odt, .doc, .docx, .ods, xls, .xlsx, .odp,.ppt, .pptx...).

Para utilizarla se necesita también una instalación de
Nextcloud/Owncloud, que será el interfaz que permitirá acceder a la
edición de los documentos. Con OnlyOffice, los documentos almacenados
en Nextcloud o en Owncloud, se abrirán en el navegador para su
lectura/edición, en lugar de mostrarse por defecto la opción de
descarga.

Instalación
-----------

Antes de proceder con la instalación es necesario decidir bajo qué
dominio o subdominio se quiere alojar la aplicación. Usaremos como
ejemplo el subdominio *onlyoffice.mydomain.com.*

Tendremos que crear un registro DNS de tipo A, que apunte a la IP del
servidor.

``onlyoffice.mydomain.com A IP.DE.TU.SERVIDOR``

Según el proveedor de dominio que tengas, la propagación de los DNS
puede tardar entré pocos minutos y unas horas. Una vez los DNS estén
propagados podrás proceder a la instalación desde el panel de control.

En la página '**Instalar aplicaciones**', al marcar la casilla 'Seleccionar'
en OnlyOffice, se mostrará un campo en el que tendrás que insertar el
nombre del dominio/subdominio que quieras utilizar para instalar la
aplicación. En el caso de nuestro ejemplo será *onlyoffice.mydomain.com*

.. figure:: img/onlyoffice/install-onlyoffice.png
   :alt: Screenshot

   Instalación de OnlyOffice.

Conectar Nextcloud / Owncloud
-----------------------------

Una vez terminado el proceso de instalación de OnlyOffice, es necesario
instalar y configurar la extensión OnlyOffice desde Nextcloud (o
Owncloud). Para ello accede con una cuenta que tenga permisos de
administración y ve a '**Aplicaciones**' > '**Oficina y texto**' y busca
'OnlyOffice', haz clic en '**Descargar y activar**'.

.. figure:: img/onlyoffice/install-oo-nc.png
   :alt: Screenshot

   Instalación de OnlyOffice en Nextcloud.

Una vez activada la aplicación tendrás que ir a **'Configuración'** > **'OnlyOffice'** e insertar dos valores:

-  La url de instalación de la aplicación con 'https://'. En el caso de
   nuestro ejemplo 'https://onlyoffice.mydomain.com'.
-  La clave secreta que autorizará tu instalación de Nextcloud a
   conectar con el servidor OnlyOffice. Esta clave está incluida en el
   correo electrónico que tu servidor ha enviado a la cuenta de
   administrador del panel de control, una vez terminada la instalación.

En esta misma página podrás también establecer otras preferencias, como
habilitar otros formatos para la edición de documentos.

.. figure:: img/onlyoffice/onlyoffice-configure.png
   :alt: Screenshot

   Configuración de OnlyOffice.

A partir de ahora, cada vez que accedas a un documento desde la sección
Files/Archivos, se abrirá en el navegador para su edición.

**Aclaración:** se instala la versión *OnlyOffice Community Edition* que tiene como limitación un máximo de 10 documentos abiertos a
la vez y un máximo de 20 conexiones simultáneas.
