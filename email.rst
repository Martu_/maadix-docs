Email
=====

Puedes añadir infinitas cuentas de correo para todos los dominios que tengas activados en tu servidor. Antes de añadir cuentas de correo electrónico, es importante que te asegures de haber configurado correctamente el dominio correspondiente. Para saberlo, consulta la `configuración del dominio </dominios>`__.

Crear cuenta
------------

En la sección **'Correos'** > **'Cuentas de Correo'** , haz clic en el botón **'Añade una cuenta nueva'** y rellena los diferentes campos del formulario que se despliega. En el campo 'Nombre de la cuenta' tendrás que insertar solo el nombre de la cuenta (sin @dominio.com) y tendrás que elegir un dominio del listado de dominios disponibles, el cual habrás activado previamente en la sección 'Dominios'. Para crear la cuenta user@example.com tendrás que insertar user en el campo 'Nombre de la cuenta' y elegir el dominio 'example.com' desde el desplegable del campo 'Dominio'.

.. figure:: img/mails/add-mail.png
   :alt: Screenshot

   Creando una cuenta de correo.

Editar cuenta
-------------

Puedes cambiar los parámetros de las cuentas de correo electrónico que
tengas activadas visitado la sección **'Correos'** > **'Cuentas de
correo'** y haciendo luego clic en el botón **'Editar cuenta'**
correspondiente a la cuenta que desees modificar.

.. figure:: img/mails/edit-email-link.png
   :alt: Edit email

   Editando una cuenta de correo.

Reenvío automático (Forward) y respuesta automática (Vacation)
--------------------------------------------------------------

En la misma página de edición de cada cuenta de correo electrónico
puedes activar el reenvío automático de todo el correo entrante a
cualquier otra dirección de correo.

Para hacerlo, tienes que activar la casilla **'Activar reenvío automático'**, insertando una dirección válida en el nuevo campo que se
despliega. En caso de que quieras que se reenvíe a múltiples cuentas,
debes separar cada una con coma (user1@example.com,user2@example.com).
Si quieres seguir recibiendo una copia de los correos entrantes en tu
cuenta actual, tendrás que incluirla en el listado.

Para activar la respuesta automática (fuera de oficina) activa la
casilla **'Activar respuesta automática'**. En el campo inferior
**'Mensaje de respuesta automática'** inserta el texto que deseas
enviar.

.. figure:: img/mails/edit-mail.png
   :alt: Forward email

   Configuración de reenvío y respuesta automática.

Eliminar cuenta
---------------

Cuando eliminas una cuenta de correo, los mensajes de sus carpetas
(recibidos, enviados, papelera...) no se borran y se guarda una copia en
tu servidor. La cuenta es desactivada, de modo que no podrá seguir
enviando o recibiendo correo. Si en un futuro vuelves a crear la misma
cuenta, recuperarás todo el contenido de sus carpetas. Para vaciar
definitivamente una cuenta de correo, puedes borrar su contenido
utilizando la interfaz webmail (si tienes la aplicación instalada) o
utilizando un cliente de correo electrónico (Thunderbird, Outlook...)
configurada con IMAP. IMAP crea una sincronización entre el cliente y el
servidor, de manera que todas las acciones efectuadas en una de las dos
partes se refleja en la otra.

Webmail - Rainloop
------------------

Si has instalado la aplicación Rainloop en tu servidor, podrás utilizar esta herramienta webmail para consultar y enviar correo electrónico desde tu navegador. Desde el panel de control mismo tienes un acceso directo a la aplicación webmail. Además de poder utilizar la dirección ``myserver.maadix.org/rainloop``, también puedes utilizar cualquier dominio propio que tengas funcionando en el servidor.

Si, por ejemplo, has configurado con éxito el dominio *example.com* para tu servidor, puedes visitar la interfaz webmail visitando ``example.com/rainloop``.

Todas las cuentas de correo electrónico activadas correctamente a través del panel de control pueden ser consultadas a través de la interfaz webmail.

.. figure:: img/mails/rainloop.png
   :alt: rainloop
   :width: 800

   Acceso por webmail (Rainloop).


Acceso admin de Rainloop
~~~~~~~~~~~~~~~~~~~~~~~~

Para evitar confusiones, anteriormente no se enviaba la contraseña de acceso al panel de administración de Rainloop después su instalación, ya que la configuración de los dominios se realizaba automáticamente y no era necesario modificar la configuración. Sin embargo, a partir de noviembre de 2020, sí se envía la contraseña de acceso. Las personas que tenían instalado Rainloop con anterioridad pueden encontrar la contraseña en ``/etc/maadix/rainloop``. La configuración de los dominios sigue siendo automática pero la configuración de los contactos es opcional y se gestiona desde este panel de administración.

Datos de acceso:
  - Url: ``myserver.maadix.org/rainloop/?admin``
  - Usuarix: ``admin``
  - Contraseña: puedes encontrarla en el correo que se te envió o en ``/etc/maadix/rainloop`` (necesitas acceder como Superusuarix para abrir este archivo).

En el panel de control de administración de Rainloop vas a poder editar varias configuraciones. **Recomendamos no editar la configuración relativa a los dominios**, ya que son configurados automáticamente cuando se añaden al panel de control de MaadiX.

.. figure:: img/mails/rainloop_admin.png
   :alt: rainloop_admin
   :width: 800

   Acceso al panel de administración de Rainloop.




Contactos en Rainloop
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La funcionalidad de 'Contactos' de Rainloop es muy útil, permite tener una libreta de direcciones y puede ser utilizada a la hora de redactar correos. Para habilitar esta funcionalidad solo hay que acceder al panel de administración de Rainloop (como se explica en el apartado anterior) y en la sección 'Contactos' habilitar la opción 'Permitir contactos'.

Si quieres que también se puedan sincronizar contactos, puedes habilitar 'Permitir sincronización de contactos (servidor externo de CardDAV)'. `Más abajo </email/#sincronizar-contactos>`__ se explica cómo hacer la sincronización con la app 'Contactos' de Nextcloud.

.. figure:: img/mails/Rainloop_contact_sqlite.png
   :alt: rainloop_contacts
   :width: 800

   Configuración de contactos en el panel de administración de Rainloop.

Además, la funcionalidad de contactos puede funcionar con SQLite o con MySQL, en el desplegable se puede seleccionar la opción que se prefiera.

Si se elige usar **SQLite** solo es necesario pulsar el botón test para comprobar que Rainloop puede escribir los contactos en la base de datos y que la configuración es correcta. Como se advierte en el mensaje, no se aconseja para un gran número de usuarixs.

Si se elige usar **MySQL**, habrá que dar algunos pasos más. Será necesario acceder por SSH al servidor, crear una base de datos y unx usuarix de MySQL con permisos para escribir en ella. Si no se tiene experiencia con MySQL se pueden seguir los siguientes pasos:

(Habiendo accediendo por SSH con la cuenta Superusuarix)

1. Acceder a MySQL

``sudo mysql``

2. Crear base de datos y usuarix con permisos de escritura:

``CREATE DATABASE rainloop;``

``GRANT ALL PRIVILEGES ON rainloop.* TO 'rainloop'@'localhost' IDENTIFIED BY 'contraseña';``

``FLUSH PRIVILEGES;``

``exit;``

Ahora habrá que indicar estos datos en el panel de administración de Rainloop, como se ve en la siguiente imagen.

.. figure:: img/mails/Rainloop_contact_mysql.png
   :alt: rainloop_contacts
   :width: 800

   Contactos configurados con MySQL.

Pulsa el botón de 'Probar' para comprobar que la configuración es correcta.

A partir de ahora, las cuentas de correo que sean consultadas en Rainloop también podrán tener una libreta de direcciones.

.. figure:: img/mails/Rainloop_addressbook.png
   :alt: Rainloop_addressbook
   :width: 800

   Libreta de direcciones en Rainloop.



Cliente de correo
-----------------

MaadiX permite que puedas consultar tu correo electrónico utilizando un cliente de correo que tengas instalado en tu ordenador (Thunderbird, Outlook...). Con tal de poder configurar tu cuenta dentro del cliente, necesitas los datos de conexión al servidor, que puedes encontrar en el Panel de Control, en el apartado **'Correos'** > **'Cuentas de correo'** , en **'Editar cuenta'** de la cuenta que se quiera configurar, y haciendo click en el botón IMAP o POP3 arriba a la derecha.

.. figure:: img/mails/EmailConf.png
   :alt: Email_details

   Configuración para cliente de correo.

Por ejemplo, en Thunderbird la configuración por IMAP quedará así:

.. figure:: img/mails/thunderbird.png
   :alt: thunderbird_conf
   :width: 700


   Ejemplo de configuración para Thunderbird.



Sincronizar contactos
---------------------

Exite la posibilidad de sincronizar tus contactos de Nextcloud tanto en `Thunderbird <https://www.thunderbird.net/>`__ (cliente de correo) como en Rainloop (webmail). La aplicación 'Contactos' no viene instalada por defecto en Nextcloud, así que procede a instalarla, si aún no lo has hecho.

.. figure:: img/mails/ContactsNC.png
   :alt: ContactsNC
   :width: 1000

   Instalación de la App "Contactos" en Nextcloud.

Una vez tengas tus contacos añadidos a la App 'Contactos', para poder sincronizarlos vas a necesitar el enlace de la libreta de direcciones. Lo puedes encontrar en Nextcloud, en **Contactos** > **Ajustes** > menú de los tres puntos > **copiar enlace**, tendrá el formato ``https://myserver.maadix.org/nextcloud/remote.php/dav/addressbooks/users/user/contacts/``

.. figure:: img/mails/EnlaceContactsNC.png
   :alt: Contact_link_NC
   :width: 400

   Enlace CardDav.



En Thunderbird
~~~~~~~~~~~~~~

Para sincronizar tus contactos de Nextcloud con Thunderbird necesitas el plug-in `CardBook <https://addons.thunderbird.net/en-US/thunderbird/addon/cardbook/?src=search>`__ .

Una vez instalado puede añadir una nueva libreta de direcciones remota de tipo CardDav como se muestra en la siguiente captura, tendrás que añadir la url de la libreta de direcciones de Nextcloud que tendrá el formato: ``https://myserver.maadix.org/nextcloud/remote.php/dav/addressbooks/users/user/contacts/`` además del nombre de la cuenta de Nextcloud y su contraseña.

.. figure:: img/mails/CardBookConf.png
   :alt: Card_book_conf
   :width: 800

   Configuración de CardBook.


En Rainloop
~~~~~~~~~~~~~~

Para sincronizar tus contactos de Nextcloud en Rainloop tienes que ir a '**Configuración**' > '**Contactos**' y en el apartado **'Sincronización remota (CardDAV)'** activar la opción 'Activar la sincronización remota' y añadir el enlace de la libreta de direcciones de Nextcloud que tendrá el formato ``https://myserver.maadix.org/nextcloud/remote.php/dav/addressbooks/users/user/contacts/`` además del nombre de la cuenta de Nextcloud y su contraseña.

.. figure:: img/mails/RainloopSync.png
   :alt: RainloopSync_conf
   :width: 800

   Configuración para sincronización de contactos en Rainloop.

Recuerda que previamente se tiene que haber activado la funcionalidad 'Contactos' desde el panel de administración de Rainloop (como se indica `aquí </email/#contactos-en-rainloop>`__) para que la configuración relativa a contactos esté disponible.

Una vez configurada la sincronización de contactos se puede ir a la libreta de direcciones e iniciar la sincronización, como se muestra en la siguiente imagen.

.. figure:: img/mails/RainloopSyncInit.png
   :alt: RainloopSync
   :width: 800

   Sincronización de contactos en Rainloop.
