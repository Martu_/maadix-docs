Aplicaciones
============

Instalar aplicaciones
---------------------

Desde el panel de control podrás instalar todas las aplicaciones disponibles en MaadiX. Puedes consultar el listado desde el apartado '**Instalar aplicaciones**'.

.. figure:: img/apps/install-apps.png
   :alt: Instalar aplicaciones

   Aplicaciones disponibles para ser instaladas.

**Ciertas aplicaciones** (como Jitsi Meet, RocketChat, Only Office Online, Collabora Online, Discourse y Mailtrain) **necesitan un dominio o subdominio propio para su instalación**. Cuando haces clic en '**Seleccionar**' aparecerá un campo en el que te lo pedirá.

.. figure:: img/apps/DominioApps.png
   :alt:

   Indicando el dominio donde instalar una aplicación.

Este dominio tienen que apuntar a la IP del servidor, para ello tendrás que añadir un registro A en el editor de zonas DNS de tu dominio (operación externa a MaadiX).

Si después de la instalación quieres modificar el dominio puedes cambiarlo en el panel del control, en la pestaña '**Mis Aplicaciones**' > '**Nombre de la Aplicación**' > '**Configurar**'.

Recuerda que **este dominio o subdominio no lo tienes que añadir en la sección 'Dominios' del panel de control**. De hecho si lo haces, ya no lo podrás utilizarlo para la instalación de una aplicación.

Desactivar aplicaciones
-----------------------

Las aplicaciones pueden ser desactivadas o reactivadas desde la página '**Mis Aplicaciones** -> **Ver todas**'.

Cuando desactivas una aplicación los datos no se borran, así que si la vuelves a activar en otro momento recuperarás la configuración anterior.

.. figure:: img/apps/view-applications.png
   :alt: Ver aplicaciones

   Aplicaciones instaladas.

Otras aplicaciones
------------------

Si quieres instalar otras aplicaciones no listadas en el panel de control de MaadiX puedes hacerlo por tu propia cuenta ya que tienes pleno acceso al sistema. Sin embargo, MaadiX no se responsabiliza de
posibles riesgos de seguridad o posibles incompatibilidades que se puedan generar.

El servicio de soporte que se presta en cualquiera de los planes de MaadiX no incluye soporte para aplicaciones adicionales.

Si necesitas una asistencia específica para la instalación y mantenimiento de aplicaciones adicionales ponte en contacto con el equipo de MaadiX para valorar un presupuesto a medida.

Archivos de configuración
-------------------------

De forma general, la configuración de las aplicaciones instaladas a través del panel de control se hará a través de sus propios paneles de administración (a través del navegador) que trae cada herramienta, sin necesidad de tocar archivos de configuración del sistema.

**Recuerda que si modificas archivos de configuración** de los servicios que MaadiX trae integrados en el sistema (como Apache, fail2ban, mysql, ssh, etc),** éstos se van a sobre-escribir con la configuración que MaadiX pone por defecto** cada vez que actualices el sistema (botón "Actualizar" del panel de control).

MaadiX funciona con `Puppet <https://es.wikipedia.org/wiki/Puppet_(software)>`__, un software para la administración de sistemas de forma "declarativa". **Puppet se ejecuta en cada actualización y resetea todas las configuraciones que no hayan sido declaradas por MaadiX**.

Si necesitas alguna configuración específica que no quieres que se sobre-escriba, ponte en contacto con el equipo de MaadiX.
