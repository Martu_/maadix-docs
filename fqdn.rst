Cambiar el nombre del servidor
===============================

El nombre del servidor es un valor (normalmente un subdominio), que
distingue de forma única una máquina en Internet. Este valor se
denomina, en términos técnicos, Fully Qualified Domain Name (FQDN).

Cuando das de alta un servidor con MaadiX, puedes elegir un nombre para
tu equipo, al que por defecto asignamos un subdominio de maadix.org.
Así, por ejemplo, si el nombre que has escogido fuese 'myserver' el
nombre completo de tu servidor será ``myserver.maadix.org``.

**Recuerda que el nombre del equipo no se puede cambiar**, piénsalo bien
cuando des de alta el servidor.

MaadiX se encarga de crear todas las configuraciones necesarias para que
el servidor funcione correctamente. Esto incluye no solo las
configuraciones del sistema, sino también las entradas DNS necesarias
para el subdominio recién creado. Sin estos registros DNS, el servidor
no podría funcionar correctamente.

Hemos elegido implementar esta forma para simplificar y hacer más rápido
el proceso de creación de los servidores, sin embargo, puedes decidir
dejar de usar el subdominio 'maadix.org' que hemos creado, para pasar a
usar tu propio dominio.

En esta página explicamos como puedes cambiar el dominio principal
asociado a tu servidor a través del panel de control, y todas las
acciones paralelas que tendrás que llevar a cabo, para que el cambio se
exitoso, y todos los servicios funcionen correctamente.

Antes de empezar
----------------

Utilicemos ``myserver.maadix.org`` como ejemplo de nombre actual,
asociado a tu servidor.

Este valor es el FQDN de tu máquina y está compuesto por dos partes:
``myserver`` [nombre del equipo] y ``maadix.org`` [dominio asociado].

Antes de proceder con el cambio, ten en cuenta que **el valor "nombre
del equipo" (myserver) no puede ser cambiado**. Solo podrás cambiar el
valor "dominio asociado". Así, por ejemplo, si el dominio que quieres
usar a partir de ahora fuera "example.com", tu nuevo FQDN será
``myserver.example.com``.

El primer paso para proceder será crear el subdominio
``myserver.example.com`` en tu proveedor de domino, con sus
correspondientes entradas DNS.

Configuración de DNS
--------------------

Esta es la parte más tediosa del proceso, pero es indispensable que la
lleves a cabo de forma correcta y antes de proceder con el cambio desde
el panel de control.

Estas configuraciones las tendrás que hacer desde el panel de gestión de
Zonas DNS, de tu proveedor de dominio. Los registros necesarios y sus
valores son los siguientes:

+--------+------------------------+-------------------------+
| Tipo   | Nombre                 | Valor                   |
+========+========================+=========================+
| A      | myserver.example.com   | IP.DE.TU.SERVIDOR       |
+--------+------------------------+-------------------------+
| MX     | myserver.example.com   | myserver.example.com.   |
+--------+------------------------+-------------------------+
| TXT    | myserver.example.com   | "v=spf1 a mx ~all"      |
+--------+------------------------+-------------------------+

Tendrás que sustituir myserver.example.com por el nuevo subdominio
(nombre de equipo + dominio que quieras utilizar),y el valor
IP.DE.TU.SERVIDOR por el valor real de la IP de tu servidor.

Una vez aplicados los cambios hay que esperar que la nueva configuración
sea propagada por la red. Este proceso puede tardar entre pocos minutos
y varias horas, dependiendo de tu proveedor de dominio. El panel de
control integra un sistema de comprobación, por lo que no arrancará el
proceso de cambio hasta que los DNS estén propagados.

Hay otro registro DNS que tendrás de activar, y cuyas instrucciones
encontrarás más abajo. Se trata del registro DKIM, que consiste en el
valor de una clave que se genera durante el proceso, por lo que solo
podrás activarlo una vez terminados los pasos que se describen a
continuación.

Cambio en el panel de control
-----------------------------

Terminada la operación anterior, accede al panel de control y ve a
Sistema -> Detalles, abajo de todo, haz clic en 'Cambiar el dominio del
servidor'.

.. figure:: img/fqdn/home2.png
   :alt: Edit server settings

   Cambiar el dominio del servidor.

Se abrirá la página para poder proceder con el cambio, y que incluye
instrucciones para la configuración de los DNS y otras advertencias a
tener en cuenta. Aconsejamos que leas detenidamente todas las
indicaciones.

Recuerda que solo podrás cambiar el dominio asociado al servidor y no el
nombre. Siguiendo el ejemplo '**myserver.maadix.org**' puede ser
cambiado por '**myserver.example.com**' o '**myserver.example.net**'
pero no '**newname.example.com**'.

El proceso es extremadamente sencillo. Solo tendrás que insertar el
nuevo dominio que quieras utilizar (por ejemplo '**example.com**') en el
formulario que se muestra al hacer clic en el desplegable 'Cambiar
dominio del servidor'.

.. figure:: img/fqdn/change-fqdn.png
   :alt: Change fqdn form

   Formulario de cambio del FQDN.

Adicionalmente, puedes activar la casilla 'Recibir Logs'. Esto te
permitirá recibir a tu cuenta de correo, informes diarios sobre el
estado del sistema, fallos en los servicios, errores o actualizaciones.
Si no la activas, estos informes se enviarán a una cuenta a la que tiene
acceso el equipo técnico de MaadiX, para que se puedan consultar en el
caso que se produzcan errores.

Podrás cambiar esta configuración en cualquier momento desde la página
de 'Notificaciones'.

Antes de empezar el proceso, el sistema comprobará que las entradas DNS
necesarias estén creadas correctamente y en caso afirmativo, procederá
con el cambio.

Esta operación es un poco más larga que otras tareas administrativas, ya
que se tienen que reconfigurar varios servicios y paquetes presentes en
el sistema. Ten paciencia.

Una vez completado el proceso recibirás un correo electrónico de
confirmación que incluirá la nueva dirección que tendrás que utilizar
para acceder al panel de control:

::

    https://myserver.example.com/cpanel

El enlace antiguo ya no será váildo.

.. figure:: img/fqdn/fqdn-process.png
   :alt: Change fqdn form

   Mensaje durante el proceso.

Configurar DKIM
---------------

Es muy importante que, una vez terminado el cambio, añadas el registro
DKIM a los DNS del dominio, desde el panel de gestión de DNS que te
proporciona tu proveedor de dominio. Este registro asegura la identidad
y legitimidad de los correos electrónicos enviados desde el servidor,
por lo que es indispensable para evitar que tus mensajes sean tratados
como SPAM.

Lo primero que necesitas es saber cuales son los valores correctos para
la entrada DNS. Esta información está ahora disponible en el panel de
control, en Sistema -> Detalles, en la sección 'Configuración DNS del
dominio del servidor'.

El registro DKIM es un registro de tipo TXT, cuyo nombre y valor
necesarios puedes copiar desde la tabla. Al consultarla, verás que en
columna '**Estado**' está marcada con un aspa en rojo. Significa que no
hay coincidencia entre el valor requerido y el valor encontrado.

.. figure:: img/fqdn/dkim-missing.png
   :alt: Dns table no dkim

   DKIM requerido.

La configuración del DKIM es la más compleja, y la sintaxis puede variar
en función de cada proveedor. En este enlace encontrarás una explicación
detallada sobre su funcionamiento y configuración, además de una
herramienta para comprobar que la entrada DNS que ha insertado es
correcta:

`Cómo configurar registro DKIM </dns/#registro-dkim>`__.

Cuando hayas terminado, y una vez los DNS se hayan propagando, la misma
tabla te devolverá los valores encontrados para el registro DKIM. Si son
correctos, el texto que anteriormente estaba marcado con un aspa en
rojo, se habrá puesto con un tick verde, marcando el éxito de la
operación.

.. figure:: img/fqdn/dkim-ok.png
   :alt: Dns table no dkim

   DKIM configurado correctamente.

Últimos pasos
-------------

Correo electrónico
~~~~~~~~~~~~~~~~~~

Si estás usando algún cliente de correo electrónico en algún dispositivo
(Thunderbird, Outlook, k9...) tendrás que cambiar el valor para el
servidor de entrada y de salida, y sustituirlo con el nuevo. Encontrarás
los nuevos valores de configuración en la página de edición de las
cuentas de correo electrónico del panel de control.

.. figure:: img/fqdn/edit-mail.png
   :alt: Email client

   Nueva configuración para clientes de correo.

VPN
~~~

Si tienes activada alguna cuenta VPN, tendrás también que volver a
cargar la configuración en el cliente que utilizas para la conexión, ya
que el certificado se ha modificado, y el anterior dejará de ser válido.
Puedes ir a la edición de la cuenta VPN y marcar que se manden de nuevo
las instrucciones de conexión (con los archivos con la nueva
configuración).
