Seguridad
=========

En el panel de control, en la pestaña 'Sistema', encontrarás la sección
'Seguridad'. Aquí vas a poder configurar diferentes opciones para
reforzar la seguridad de tu servidor.

SSH
---

En el apartado 'SSH' podrás **configurar el puerto para las conexiones
SSH**. Elige del desplegable el puerto que quieras entre el 2001 y el
2010 (por defecto, tendrás el puerto 22). Una vez cambiado, las
conexiones por SSH tendrán que incluir el parámetro con el nuevo puerto
designado, por ejemplo:

``ssh -p 2001 user@minombreenmaadix.maadix.org``

Cambiar el puerto de la conexión SSH refuerza tu seguridad porque la
mayoría de atacantes hacen intentos de conexión al puerto 22 (el puerto
por defecto para SSH).

Además, puedes marcar la casilla '**Deshabilitar acceso SSH con
contraseña**' para que las conexiones al servidor solo puedan hacerse
mediante claves SSH, no con contraseña.

Esta opción protegerá de cualquier intento conexión por SSH por parte de
cuentas desconocidas que traten de probar diferentes contraseñas.

Después de deshabilitar acceso SSH con contraseña solo las cuentas a las
que les hayas añadido su clave pública (Usuarixs > Cuentas ordinarias >
Clave SSH) van a poder conectase al servidor. Esto aplica tanto a
conexiones por SSH o como por SFTP.

Recuerda que puedes añadir la clave pública de una cuenta tanto al
crearla como al editarla. A la cuenta Superusuarix también le puedes
añadir clave SSH. Desde MaadiX, lo recomendamos especialmente para esta
cuenta.

En el caso de que no deshabilites el acceso con contraseña se va a poder
acceder al servidor haciendo uso tanto de contraseña como de clave SSH.
Esto puede ser cómodo para que lxs usuarixs no tengan que tipear la
contraseña pero no añadirá seguridad al sistema.

Si tienes dudas sobre como generar y utilizar claves SSH puedes
consultar `este
tutorial <https://desarrolloweb.com/articulos/crear-llaves-ssh.html>`__.

.. figure:: img/security/ssh-sec.png
   :alt:

   Apartado de seguridad para SSH.

TLS
---

TLS (en inglés: `Transport Layer
Security <https://es.wikipedia.org/wiki/Seguridad_de_la_capa_de_transporte>`__)
es un protocolo criptográfico que proporciona comunicaciones seguras en
internet. Hay diferentes versiones de este protocolo (TLS 1.0, TLS 1.1,
TLS 1.2) y actualmente `lo
recomendado <https://www.packetlabs.net/tls-1-1-no-longer-secure/>`__ es
utilizar TLS 1.2. Las versiones anteriores se consideran inseguras ya
que contienen vulnerabilidades de seguridad, corregidas en la versión
TLS 1.2.

En el apartado **Correo electrónico** puedes configurar qué versiones de
TLS quieres que tu servidor de correo admita.

.. figure:: img/security/mail-sec.png
   :alt:

   Apartado de seguridad sobre versiones TLS (correo electrónico).

Aunque se recomienda usar solo usar TLS 1.2, se da la opción de también
soportar versiones anteriores. Esto da la posibilidad de recibir o
enviar correos a servidores que hagan uso de estas versiones (por
ejemplo, servidores de correo antiguos que aún no tengan habilitado TLS
1.2).

Puedes cambiar las versiones TLS soportadas cuando lo desees.

De la misma manera, en el apartado **Servidor web** vas a poder
establecer qué versiones de TLS quieres que tu servidor web soporte.
Igualmente, solo TLS 1.2 es la opción recomendada.

.. figure:: img/security/web-sec.png
   :alt:

   Apartado de seguridad sobre versiones TLS (servidor web).

En este caso, serán los navegadores web los que tendrán que soportar TLS
1.2 para poder consultar las páginas webs alojadas en tu servidor. Esto
será así en la mayoría de navegadores, solo en caso de navegadores
antiguos que solo soporten versiones anteriores se podrían dar
problemas.

Se da la opción de elegir entre las versiones soportadas para adaptarse
a todas las necesidades. Podrás cambiar esta configuración cuando lo
desees.

Hasta que no le des al botón 'Guardar' no se realizarán los cambios.
