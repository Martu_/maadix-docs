.. MaadiX Documentación documentation master file, created by
   sphinx-quickstart on Wed Sep  2 13:28:18 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :hidden:

   panel-de-control
   updates
   security
   users
   dominios
   dns
   fqdn
   trash
   fail2ban
   create-web
   wordpress
   applications
   email
   etherpad
   jitsi-meet
   lool
   mail-man
   mailtrain
   mysql
   ncloud
   onlyoffice
   coturn
   vpn


Tutoriales y documentación
==========================

En esta web encontrarás la documentación completa sobre el uso de `MaadiX <https://maadix.net/>`__.


¿Qué es MaadiX?
---------------

`MaadiX <https://maadix.net/>`__ es una herramienta que permite habilitar con un clic aplicaciones
de código abierto en un servidor propio. Además, las puedes administrar
a través de una interfaz gráfica sin necesidad de **conocimientos
técnicos** o **grandes inversiones**. En esta guía encontrarás
indicaciones para el uso de la interfaz gráfica de administración.

¿Cómo funciona?
---------------

MaadiX integra una serie de configuraciones de sistema estandarizadas,
que permiten administrar servidores virtuales propios y aplicaciones
desde una interfaz gráfica fácil e intuitiva. Permite instalar
aplicaciones avanzadas como Jitsi Meet, OpenVPN, Etherpad, servidor de
correo, Mailman, Let's Encrypt u Nexctloud entre otras, todo en un
simple clic. Con MaadiX, podrás disponer de una nube propia
independiente, privada y segura.

Panel de Control
----------------

El panel de control es la interfaz que permite ejecutar distintas tareas
de administración y mantenimiento sin necesidad de ejecutar comandos en
la terminal. Podrás instalar y administrar aplicaciones, crear cuentas,
dominios, cuentas de correo electrónico, otorgar acceso al sistema a
otras personas con diferentes tipos de privilegios, ver estadísticas
básicas del sistema entre otras operaciones.

Los datos
---------

Todos los servidores tienen instalado un directorio donde se almacenarán
los datos de cuentas, dominios, cuentas de correo electrónico y
aplicaciones; técnicamente hablando, se trata de un directorio
`OpenLDAP <https://es.wikipedia.org/wiki/OpenLDAP>`__.

El panel de control es la interfaz gráfica para administrar este
directorio OpenLDAP y el sistema sacará del mismo la información
necesaria para su configuración. De esta forma, el panel de control no
necesita privilegios para escribir directamente esta información en el
sistema.

Haciendo uso de tareas programadas en el sistema (*cronjobs*) se
complueba si hay cambios en el directorio de OpenLDAP (nuevas
configuraciones o nuevas aplicaciones para instalar) y en caso
afirmativo se aplican los cambios a través de
`Puppet <https://es.wikipedia.org/wiki/Puppet_(software)>`__. Esta
implementación de Puppet es lo que permite poder aplicar cambios en el
sistema sin tener acceso a los servidores ni a datos sensibles de lxs
usuarixs.

Puedes encontrar más detalles sobre la tecnología que usa Maadix
`aquí <https://maadix.net/es/tecnologia>`__.

Cambios desde la Versión 1.0
----------------------------

Si has actualizado la versión de MaadiX desde una instalación anterior,
notarás algunos cambios no solo en la apariencia de la interfaz gráfica,
sino también en alguna funcionalidad:

-  A partir de esta versión se ha eliminado la posibilidad de visitar el panel de control desde cualquier dominio creado en el servidor. Solo será disponible a través del dominio principal del servidor (FQDN).

-  Se ha incrementado el número mínimo de caracteres requeridos para las contraseñas en 10.
