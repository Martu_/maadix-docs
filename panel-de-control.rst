Panel de control
================

Alta del servidor
-----------------

Cuando el proceso de creación de una máquina virtual en MaadiX termina, el mismo servidor recién creado envía un correo de notificación a la cuenta de correo electrónico que se ha designado como administradora.

El correo contiene instrucciones sobre cómo acceder al servidor, incluyendo las contraseñas. Dichas contraseñas son generadas localmente por el servidor, de modo que nadie más que él las conoce.

Sin embargo (al menos por el momento), el proceso de envío de contraseñas mediante correo se efectúa en texto plano, lo que hace absolutamente indispensable que se proceda inmediatamente a cambiarlas por cuestiones de seguridad. Es por eso que MaadiX fuerza el cambio de las contraseñas a través de un proceso de activación en el momento de la primera autenticación en al panel de control.

Activación
----------

La primera vez que se accede al panel de control solamente se mostrará
la página de activación. En esta página se tendrá que proceder al cambio
de la contraseña de dos diferentes cuentas del sistema:

-  La cuenta de administración para el panel de control.
-  La cuenta de Superusuarix (root del sistema).

Hasta que no se complete este proceso, las otras secciones del panel de
control no serán accesibles y la cuenta Superusuarix no podrá acceder al
sistema ni por SSH ni por SFTP.

Esta es una importante medida de seguridad, que también tiene que ser
aplicada para cualquier otra aplicación que se haya instalado y que
requiera un nombre de usuarix y contraseña.

Es sumamente importante que el correo electrónico asociado a la cuenta
de administración del panel de control sea válido y que tengas acceso al
mismo, ya que el servidor enviará a esta cuenta todas las
notificaciones, indicaciones e instrucciones para recuperar la clave de
acceso del panel de control en caso de que la olvidaras.

Detalles
--------

La ventana de inicio del panel de control muestra información
estadística interna acerca del uso de recursos en el sistema. En ella
encontrarás también información sobre la configuración DNS para el
dominio principal de tu servidor.

.. figure:: img/PanelControl/panel-de-control.png
   :alt: Screenshot

   Detalles del uso de recursos en el sistema.


Recuperar contraseña
--------------------

**Recuerda que el equipo de MaadiX no tiene acceso a estos datos y no
puede hacer este proceso por ti.**

Si olvidas la contraseña de la cuenta de administración del panel de
control tienes que iniciar el proceso de recuperación de contraseña
haciendo clic en "He olvidado mi contraseña".

Entonces se te pedirá el nombre de la cuenta que tiene permisos de
administración (se te envió por correo cuando activaste el servidor) y
además tendrás que poner el correo electrónico asociado a esta cuenta.

.. figure:: img/PanelControl/recover-pass.png
   :alt:

   Solicitud de recuperación de contraseña.

Si estos datos son correctos, se te enviará un correo electrónico para
iniciar el proceso de recuperación de la contraseña.

En el correo electrónico se te indicará un código de verificación y el
enlace donde tienes que insertarlo. Allí también se te pedirá la nueva
contraseña que quieres asignar.

.. figure:: img/PanelControl/verificacion.png
   :alt:

   Verificación y cambio de contraseña.

Este proceso de cambio de contraseña puede durar hasta 10 minutos.

Cuando el proceso termine recibirás un nuevo correo electrónico
avisándote de que el proceso ha terminado y ya puedes acceder al panel
de control con la nueva contraseña.

Importante: **no podrás acceder con la nueva contraseña hasta que no
recibas el segundo correo electrónico que te lo indica**, puede tardar
unos minutos.
