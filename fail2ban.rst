Fail2ban
========

Fail2ban es un software que se usa para prevenir intentos masivos de
conexión, es software libre y en MaadiX viene instalado por defecto. Su
función básica es evitar intrusiones que puedan venir de ataques de
fuerza bruta (ataques que consisten en probar miles de intentos de
conexión para tratar de dar con una combinación user/password válida).

Funciona de la siguiente manera: **cuando hay un cierto número de
intentos fallidos de acceso al servidor, fail2ban bloquea la dirección
IP** desde la cuál se intenta el acceso.

En Maadix, fail2ban está activado para los siguientes servicios:

-  apache: el servidor web para las aplicaciones web, nextcloud,
   discourse, etc
-  sshd: el protocolo de comunicación remoto y seguro con nuestro
   servidor con el que acceder a su línea de comandos.
-  ssh-ddos: implementación específica de ssh para protegerlo de ataques
   de denegación de servicio distribuidos (ddos).
-  dovecot: servidor IMAP/POP3 para conectar a las cuentas de correo.
-  mxcp: la interfaz gráfica de MaadiX (El panel de control).
-  sasl: framework de seguridad instalado en el servidor para
   sincronizar y autenticar protocolos de conexión y autenticación.

En fail2ban la protección para cada uno de estos servicios se les llama
*jails*.

Desbloquear una IP
------------------

Puede ocurrir que una persona falle reiteradamente al insertar la
contraseña tratando de acceder a un servicio y que fail2ban la bloquee.
El tiempo de bloqueo son 12h, excepto para el panel de control que solo
será 1h.

Cuando ocurre esto (y no se quiere esperar 12h para recuperar el
acceso), se tendrá que entrar por SSH al servidor para desbloquear la
dirección IP que ha sido bloqueada.

Se necesitarán ejecutar comandos para los que se requieren permisos de
administración del sistema, por lo que será necesario acceder con la
cuenta Superusuarix:

``ssh superusuarix@nombreservidor.maadix.net``

**Nota**: si es tu propia IP la que está bloqueada no vas a poder
acceder, te devolverá un error 'Connection Refused'. Te recomendamos que
te conectes a una VPN, a una red distinta, o que uses la conexión de
datos de un teléfono móvil, para que así puedas hacer el login desde una
IP distinta a la bloqueada.

Si utilizas la conexión VPN del mismo servidor MaadiX al que quieres
acceder, la forma de obviar el bloqueo es acceder utilizando la IP del
servidor OpenVpn con el siguiente comando: ``ssh username@10.8.0.1``

Para desbloquear una IP primero tenemos que averiguar cuál es la IP en
cuestión. Puedes ir a cualquier buscador para consultar "cual es mi ip",
cientos de resultados mostrarán la dirección IP de tu conexión (por
ejemplo https://cualesmiip.com).

Todas las personas de una misma oficina o una misma vivienda, que están
conectadas al mismo router van a salir a Internet con la misma IP (IP
pública), por ello puede ocurrir que toda una oficina o vivienda se
quede sin acceso al servidor (o alguno de sus servicios).

Una vez ya sabemos la IP bloqueada, se puede desbloquearla con el
siguiente comando:

``sudo fail2ban-client set <JAIL> unbanip <IP_A_DESBLOQUEAR>``

Aquí te dejamos **algunos ejemplos** para desbloquear la IP 4.4.4.4:

-  Para conexiones SSH y SFTP:

``sudo fail2ban-client set sshd unbanip 4.4.4.4``

-  Para Nextcloud, Owncloud, Rainloop (webmail) u otra aplicación que
   funcione sobre Apache:

``sudo fail2ban-client set apache unbanip 4.4.4.4``

-  Para el panel de control:

``sudo fail2ban-client set mxcp unbanip 4.4.4.4``

-  Para imap y pop3 (acceso a los correos electrónicos):

``sudo fail2ban-client set dovecot unbanip 4.4.4.4``

-  Para smtp (enviar correos electrónicos):

``sudo fail2ban-client set sasl unbanip 4.4.4.4``

Las *jails* activadas por defecto en MaadiX son: sshd, ssh-ddos, apache,
dovecot, mxcp y sasl.

Para comprobar el estado de cada *jail* y ver qué IPs están bloqueadas,
se puede ejecutar:

``sudo fail2ban-client status <JAIL>``

Para comprobar el estado de fail2ban y qué *jails* están activadas, se
puede ejecutar:

``sudo fail2ban-client status``

Los logs de fail2ban se pueden revisar en: ``/var/log/fail2ban.log``

Si se quiere investigar más sobre fail2ban, puedes consultar la `página
oficial <https://www.fail2ban.org>`__ de Fail2ban.
