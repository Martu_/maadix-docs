MySQL
==============

Cada máquina virtual dispone de un servidor MySQL propio.

En el correo electrónico de activación del servicio se envían las
credenciales de una cuenta de MySQL con todos los privilegios. Por
razones de seguridad, se recomienda cambiarlas cuando se accede por
primera vez al servidor.

Si se ha elegido no instalar la aplicación phpMyAdmin, solamente se
podrán administrar las bases de datos y sus cuentas a través de la
consola de comandos (terminal). En caso contrario, se puede utilizar
esta aplicación para la gestión de las bases de datos y de otras
cuentas.

phpMyAdmin
----------

Se trata de una aplicación externa al panel de control, que permite
administrar bases de datos y cuentas MySQL desde una interfaz gráfica.

Por razones de seguridad, esta aplicación está protegida con una doble
contraseña. Únicamente las cuentas que tengan activado phpMyAdmin podrán
acceder a ella.

Puedes activar este acceso en el momento de crear nuevas cuentas, o
editando el perfil de cuentas ya existentes marcando la casilla 'Activar
phpMyAdmin'.

.. figure:: img/mysql/enablePhpmyadmin.png
   :alt: Screenshot

   Activación de cuenta para phpMyAdmin.

Primera contraseña
------------------

Cuando se accede a la aplicación phpMyAdmin, se muestra la primera
autenticación con una ventana emergente. En este primer formulario se
tienen que insertar las credenciales de una cuenta que tenga activada el
servicio phpMyAdmin (Advertencia: no se trata de las credenciales de una
cuenta de MySQL con acceso a bases de datos).

.. figure:: img/mysql/privateAreaPhpmyadmin.png
   :alt: phpMyAdmin private area
   :width: 70%

   Petición de credenciales de la cuenta activada para phpMyAdmin.

Segunda contraseña
------------------

Una vez efectuada satisfactoriamente la primera autenticación, se
muestra la interfaz de la aplicación phpMyAdmin, que solicitará una
cuenta MySQL. Por defecto existe una cuenta MySQL cuyo nombre y
contraseña está incluido en el correo electrónico que se envía en el
momento de activar el servidor.

.. figure:: img/mysql/phpmyadmin.png
   :alt: phpMyAdmin

   Acceso a phpMyAdmin con cuenta de MySQL.

Por razones de seguridad, es muy recomendable cambiar la contraseña de
esta cuenta. Puedes hacerlo desde la misma aplicación:

.. figure:: img/mysql/phpmyadmin-chpswd.png
   :alt: phpMyAdmin change password

   Cambio de contraseña.

Es buena práctica crear una cuenta MySQL diferente por cada base de
datos, y otorgarle de este modo permisos solamente sobre una y no sobre
todas las bases de datos que tengas creadas.

Tanto las bases de datos como las cuentas MySQL y sus contraseñas se
pueden crear y administrar desde phpMyAdmin.

Por defecto, sólo la cuenta de MySQL que se crea con la activación del
servidor (cuyas credenciales se mandan por email) tiene los privilegios
necesarios para crear nuevas bases de datos, nuevas cuentas y otorgar
permisos a cada una de ellas.

En caso de que te quedaran dudas, siempre puedes consultar la
documentación oficial para el uso de phpMyAdmin en el `siguiente
enlace <https://www.phpmyadmin.net/docs/>`__.
