Dominios
========

Primer paso: consigue tu dominio
--------------------------------

Toda web o aplicación necesita un buen dominio. Si ya dispones de un
dominio propio, MaadiX te permite activarlo fácilmente y configurarlo
para usarlo en tus aplicaciones.

Si todavía no dispones de dominio propio, aquí te dejamos algunos
proveedores de ejemplo donde puedes registrarlo de manera sencilla
(¡pero hay muchísimos `más <https://www.icann.org/registrar-reports/accreditation-qualified-list.html>`__!):

-  `gandi.net <https://www.gandi.net/>`__
-  `Njal.la <https://njal.la/>`__

Sabemos que elegir un buen nombre no siempre es fácil. Por eso, mientras
decides cual va a ser tu dominio, puedes seguir usando MaadiX y la
mayoría de sus aplicaciones usando https://minombreenmaadix.maadix.org,
donde 'minombreenmaadix' coincide con el nombre que elegiste al adquirir
tu servidor MaadiX.

Sin embargo, algunas aplicaciones tales como Jitsi Meet, RocketChat,
Only Office Online, Collabora Online, Discourse, Mailtrain así como el
servidor de correo electrónico y las listas de correo podrás utilizarlas
solo si dispones de un dominio propio.

Si ya dispones de un dominio propio, sigue las instrucciones de a
continuación para activarlo.

Recuerda que **MaadiX no es un proveedor de dominios ni ofrece un editor
de zonas DNS.**

Activa tu dominio
-----------------

Normalmente solo necesitas activar dominios para páginas o aplicaciones
webs y correos electrónicos.

**Para Jitsi Meet, RocketChat, Only Office Online, Collabora Online,
Discourse, Mailtrain no necesitas añadir los dominios en el panel de
control**, solo indicarlos en el campo requerido para instalación, como
se muestra `aquí </applications/#instalar-aplicaciones>`__. En el caso
de las listas de correo, es necesario añadir los dominios a través de
Mailman, como se indica `aquí </mail-man/#configurar-dominio>`__.

Para activar un dominio tienes que ir a tu panel de control, a la
sección '**Dominios**' -> '**Añadir un dominio nuevo**'.

Se desplegará un formulario con los siguientes campos:

-  **Nombre del dominio**: introduce el nombre completo de tu dominio o
   subdominio (por ejemplo: example.com o docs.example.com).

Por cada dominio o subdominio que actives, se creará una carpeta en tu
servidor cuya ubicación será ``/var/www/html/example.com/``. Debes subir
tu web o aplicación a esta carpeta para que sea accesible desde el
navegador visitando *example.com*

-  **Activar servidor de correo para este dominio**: si quieres usar el
   servidor de correo interno para el dominio que estás creando, esta
   opción tiene que estar activada. Si de lo contrario quieres que el
   correo electrónico sea gestionado por otro servidor externo, déjala
   desactivada. Podrás cambiar esta opción en cualquier momento desde la
   página de edición del dominio.

-  **Webmaster**: puedes asignar una cuenta webmaster (cuenta
   administradora de la web) en cada dominio o subdominio que actives en
   MaadiX.

Podrá ser webmaster cualquier cuenta ordinaria a la que se le haya
activado el acceso por solo por SFTP o por SFTP y SSH. Haciendo clic en
el desplegable 'Asignar Webmaster' se listarán todas las cuentas
ordinarias que pueden asignar como webmaster.

.. figure:: img/domains/add-domain.png
   :alt: Añadir dominio

   Añadir dominio

En el caso no te aparezca ninguna tendrá que crearlas antes desde la
sección 'Usuarixs' > 'Cuentas ordinarias'. `Aquí </users/#cuentas-ordinarias>`__ puedes leer más sobre las cuentas
ordinarias y qué implican los accesos por SSH y SFTP que se les pueden
activar.

La cuenta webmaster tendrá permisos para crear, borrar o modificar los
archivos dentro de la carpeta ``/var/www/html/example.com/``, donde
podrá crear la aplicación web.

Recuerda que desde la versión 201901, para evitar brechas de seguridad,
ya **no se puede asignar como webmaster la cuenta Superusuarix.**


Configura los DNS de tu dominio para que apunten a tu servidor
--------------------------------------------------------------

Tu servidor incluye un sistema que comprueba automáticamente si tu
dominio está apuntando correctamente a tu servidor. En caso afirmativo,
el mismo sistema procederá con la creación de todas las configuraciones
necesarias. En caso contrario, volverá periódicamente a hacer la misma
comprobación hasta conseguir una respuesta afirmativa. Para saber si el
proceso de activación y configuración del dominio ha terminado con éxito
consulta el icono **Servidor web** de la página **Ver Dominios**. Si el
icono está en verde (Activado) la configuración para poder crear una web
se ha llevado a cabo satisfactoriamente.

.. figure:: img/domains/web-server-enabled.png
   :alt: Servidor web activado

   Servidor web activado

Para que tu dominio apunte hacia tu servidor, debes modificar sus DNS.
Los servidores DNS (Domain Name System, Sistema de Nombre de Dominios)
son los que transforman los nombres de dominio, pensados para la
comprensión humana, en números que corresponden a las direcciones IP de
las diferentes máquinas conectadas y accesibles públicamente en
Internet.

Haciendo clic en "Ver DNS" en la columna de tu dominio, encontrarás las
configuraciones requeridas para que tu dominio funcione tanto para tu
aplicación web (Registro A) como para tu servidor de correo (Registro MX
y SPF).

.. figure:: img/domains/view-dns-link.png
   :alt: View DNS

   Ver DNS

Debes introducir estos datos en la sección correspondiente de la
configuración de DNS dentro del **área de cliente de tu proveedor de
dominio** (esta fase tienes que completarla fuera de tu servidor
MaadiX). Seguramente habrá un enlace o pestaña, quizás en el menú, que
diga algo como *DNS*, *Editar registros DNS* o *Editar zona DNS*. Puedes
consultar la sección `DNS </dns>`__ para obtener instrucciones detalladas
sobre los diferentes tipos de registros necesarios para un correcto
funcionamiento de todos los servicios.

.. figure:: img/domains/required-dns.png
   :alt: Required DNS

   DNS requeridos

Una vez hechos los cambios, vuelve a consultar la página de
configuración de DNS en el panel de control de tu servidor MaadiX,
haciendo clic en "Ver DNS" en la columna del dominio. Recuerda que el
proceso de propagación de los nuevos DNS puede tardar hasta 48 horas, de
modo que es normal que durante un tiempo la configuración siga
resultando incorrecta aunque la hayas cambiado.

HTTPS
-----

Todos los dominios que actives a través del panel de control tendrán
siempre activado un certificado SSL y serán accesibles a través de la
dirección: https://tudominio.com

La creación y configuración de los certificados está automatizada y se
completa junto con todo el proceso de activación y configuración de
dominios en tu sistema utilizando `Let's
Encrypt <https://letsencrypt.org>`__.

No necesitas llevar a cabo ninguna configuración adicional para activar
HTTPS para tu dominio. Los certificados tienen validez de tres meses y
se renuevan automáticamente.

Subir tu web o aplicación al dominio propio
-------------------------------------------

Una vez aparezca el check verde "Activado" para el Servidor Web de tu
dominio, ya puedes subir los archivos de tu aplicación web a la recién
creada carpeta ``/var/www/html/example.com/``. Puedes hacerlo muy
fácilmente con un cliente SFTP (por ejemplo,
`Filezilla <https://filezilla-project.org/>`__) o a través de una
conexión SSH (por ejemplo, con los comandos scp o rsync), según los
permisos de acceso que tenga la cuenta webmaster asignada a este
dominio. Una vez los archivos estén ahí, podrás visitarlos desde el
navegador en tu dominio *example.com*.

Puedes encontrar más indicaciones aquí: `crea tu web o
aplicación </create-web>`__.

Empieza a usar tu servidor de correo
------------------------------------

Si has activado la casilla 'Activar servidor de correo para este
dominio', también puedes empezar a usar tu servidor de correo
electrónico. Entra en el apartado *Correos > Cuentas de correo* para
abrir nuevas cuentas, haciendo clic en el botón '*Añade una cuenta
nueva*' que encontrarás arriba a la derecha de esta página. Recuerda que
los registros MX y SPF tienen que estar correctamente configurados para
que apunten a tu servidor.

Puedes encontrar más indicaciones aquí: `crea y gestiona cuentas de
correo </email>`__.
